import React from 'react';
import ReactDOM from 'react-dom';

import renderer from 'react-test-renderer'
import { shallow, mount } from 'enzyme';

import App, { addOne } from './App';

describe('basic functions', () => {
  it('adds one', () => {
    expect(addOne(0)).toEqual(1);
    expect(addOne(4)).toEqual(5);
  })

  it('handles other inputs', () => {
    expect(addOne()).toEqual(0)
  })
})

describe('App Component', () =>{
  it('renders without crashing', () => {
    // let div = document.createElement('div');
    // ReactDOM.render(<App />, div);
    // ReactDOM.unmountComponentAtNode(div);

    const component = shallow(<App />)
  });

  it('renders properly', () => {
    const component = renderer.create(<App />);
    let snapshot = component.toJSON();

    expect(snapshot).toMatchSnapshot();
  })

  it('decreases counter on click', () => {
    const component = shallow(<App />);

    component.setState({counterValue:0})
    component.find('button').at(0).simulate('click');

    expect(component.state('counterValue')).toEqual(-1);
  })

  it('decreases counter on click', () => {
    const component = shallow(<App />);

    component.setState({counterValue:0})
    component.find('#incrementButton').simulate('click');

    expect(component.state('counterValue')).toEqual(1);
  })

  it('decreases counter on click', () => {
    const component = shallow(<App />);

    component.setState({counterValue:100})
    component.find('button').at(2).simulate('click');

    expect(component.state('counterValue')).toEqual(0);
  })

  it('calls componentDidMount', () => {
    const spy = jest.spyOn(App.prototype, 'componentDidMount');
    const component = mount(<App />);

    expect(spy).toHaveBeenCalledTimes(1);
  })
})