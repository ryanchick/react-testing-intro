import React, { Component } from 'react';
import axios from 'axios';

import './App.css';

import Counter from './Counter';

export function addOne(num){
  return typeof num === 'number' ? num + 1 : 0;
}

class App extends Component {
  constructor() {
    super();

    this.state = {
      counterValue: 0,
      asyncData:[],
    };
  }

  componentDidMount(){
    axios.get('http://randomURL.com/data')
      .then((res) => {
        this.setState({
          asyncData:res.data
        })
      })
      .catch(err => {
        console.error(err)
      })
  }

  onIncrement = () => {
    this.setState({
      counterValue:addOne(this.state.counterValue),
    })
  }
  
  onDecrement = () => {
    this.setState({
      counterValue:this.state.counterValue - 1
    })
  }

  resetCounter = () => {
    this.setState({
      counterValue:0
    })
  }

  render() {
    let { counterValue } = this.state;
    return (
      <div className="App">
        <h1>Counter App</h1>

        <Counter value={counterValue}></Counter>

        <button
          className="counter-button"
          type="button"
          onClick={this.onDecrement}
        >
          Decrement
        </button>

        <button
          id="incrementButton"
          className="counter-button"
          type="button"
          onClick={this.onIncrement}
        >
          Increment
        </button>

        <button
          className="counter-button"
          type="button"
          onClick={this.resetCounter}
        >
          Excrement
        </button>
      </div>
    );
  }
}

export default App;
