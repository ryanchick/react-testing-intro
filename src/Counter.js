
import React from 'react';

const Counter = ({ value, ...props }) => {
    let color = 'black'
    if(value > 0){
        color = 'green'
    } else if (value < 0){
        color = 'red'
    };
    return (
        <p style={{fontSize:24, color}}>{value}</p>
    )
}

export default Counter;