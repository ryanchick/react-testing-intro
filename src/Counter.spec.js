import Counter from './Counter'
import React from 'react';
import { shallow, mount } from 'enzyme';
import renderer from 'react-test-renderer'

it('renders without crashing', () => {
    const counter = shallow(<Counter />)
})

it('renders properly', () => {
    const component = renderer.create(<Counter value={0} />);
    let snapshot = component.toJSON();

    expect(snapshot).toMatchSnapshot();
})

it('shows green text when positive', () => {
    let component = shallow(<Counter value={3} />)
    let style = component.find('p').prop('style');

    expect(style).toHaveProperty('color','green')
})

it('shows red text when positive', () => {
    let component = shallow(<Counter value={-67} />)
    let style = component.find('p').prop('style');

    expect(style).toHaveProperty('color','red')
})